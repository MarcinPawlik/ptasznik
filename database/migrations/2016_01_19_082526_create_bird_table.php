<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBirdTable extends Migration
{
    /**
    * Run the migrations.
    */
   public function up()
   {
       Schema::create('birds', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('user_id'); //crtl
      $table->integer('central_id'); //crtl
      $table->dateTime('date'); //data i godzina zaobroczkowania, mozna uzyc jakis ladny bootstrapowy datapicker
      $table->string('nrob', 8); //numer obroczki
      $table->integer('species_id'); //crtl
      $table->integer('stat_id');
      $table->integer('age_id');
      $table->enum('sex', ['M', 'F']);
      $table->integer('method_id');
      $table->decimal('dcg', 4, 1);
      $table->decimal('dz', 4, 1);
      $table->decimal('wd', 4, 1);
      $table->decimal('skok', 4, 1);
      $table->decimal('szp', 4, 0);
      $table->decimal('skrz', 3, 0);
      $table->decimal('wag', 4, 0);
      $table->decimal('fat', 1, 0);
      $table->integer('color_id');
      $table->string('kolobr', 5);
      $table->string('spec1', 4);
      $table->string('spec2', 4);
      $table->string('spec3', 4);
      $table->string('spec4', 4);
      $table->string('l1', 10);
      $table->string('l2', 10);
      $table->string('krew', 3);
      $table->string('obr', 30);
      $table->string('msce', 40);
      $table->string('msce2', 30);
      $table->integer('state_id');
      $table->decimal('sz', 6, 4);
      $table->decimal('dl', 6, 4);
      $table->string('uwagi', 255);
      $table->string('dctrl', 10);
      $table->string('dnrob', 8);
      $table->string('lp', 2);
      $table->string('rnrob', 8);
      $table->string('r1ctrl', 10);
      $table->string('r1nrob', 7);
      $table->string('r2ctrl', 10);
      $table->string('r2nrob', 8);

      $table->timestamps();
  });
   }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
      Schema::dropIfExists('birds');
  }
}
