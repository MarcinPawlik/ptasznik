<?php

use Illuminate\Database\Seeder;

class StatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Stat::create(['code' => 'O', 'description' =>	'założenie nowej obrączki']);
      App\Stat::create(['code' => 'C', 'description' =>	'kontrola - ptak z obcą obrączką lub wiadomość miejscowa długoterminowa, stwierdzony pierwszy raz']);
      App\Stat::create(['code' => 'CD', 'description' =>	'kontrola - dołożenie drugiej (plastikowej) obrączki']);
      App\Stat::create(['code' => 'CZ', 'description' =>	'zamiana obcej obrączki (przeobraczkowanie)']);
      App\Stat::create(['code' => 'CM', 'description' =>	'ptak martwy z obcą obraczką']);
      App\Stat::create(['code' => 'CR', 'description' =>	'ptak z obcą obraczką schwytany ponownie - ale nie tego samego dnia']);
      App\Stat::create(['code' => 'R', 'description' =>	'retrap własny']);
      App\Stat::create(['code' => 'RM', 'description' =>	'martwy retrap własny']);
      App\Stat::create(['code' => 'M', 'description' =>	'ptak martwy bez obrączki']);
      App\Stat::create(['code' => 'D', 'description' =>	'retrap dzienny - ponowne schwytanie ptaka w ciągu tego semego dnia (także dotyczy kontroli)']);
      App\Stat::create(['code' => 'DM', 'description' =>	'matrwy retrap dzienny']);
      App\Stat::create(['code' => 'B', 'description' =>	'brak obraczki (zgubiona, zła, uszkodzona)']);

    }
}
