<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(ColorTableSeeder::class);
        $this->call(MethodTableSeeder::class);
        $this->call(AgeTableSeeder::class);
        $this->call(StatTableSeeder::class);
        $this->call(CentralTableSeeder::class);
        $this->call(SpeciesTableSeeder::class);
        $this->call(BirdTableSeeder::class); //!zasze na koncu !
    }
}
