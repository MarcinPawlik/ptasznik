<?php

use Illuminate\Database\Seeder;

class CentralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Central::create(['code' => 'ABT', 'country' => 'ALBANIA', 'name' =>	'TIRANA']);
      App\Central::create(['code' => 'AUV', 'country' => 'AUSTRIA', 'name' =>	'VIENNA']);
      App\Central::create(['code' => 'BGS', 'country' => 'BULGARIA', 'name' =>	'SOFIA']);
      App\Central::create(['code' => 'BLB', 'country' => 'BELGIUM', 'name' =>	'BRUXELLES']);
      App\Central::create(['code' => 'BYM', 'country' => 'BELARUS', 'name' =>	'MINSK']);
      App\Central::create(['code' => 'CIJ', 'country' => 'CHANNEL ISLANDS', 'name' =>	'JERSEY']);
      App\Central::create(['code' => 'CYC', 'country' => 'CYPRUS', 'name' => 'NICOSIA']);
      App\Central::create(['code' => 'CYK', 'country' => 'CYPRUS; NORTH', 'name' =>	'KUSKOR']);
      App\Central::create(['code' => 'CZP', 'country' => 'CZECH REPUBLIC', 'name' =>	'PRAHA']);
      App\Central::create(['code' => 'DEH', 'country' => 'GERMANY', 'name' =>	'HIDDENSEE']);
      App\Central::create(['code' => 'DER', 'country' => 'GERMANY', 'name' =>	'RADOLFZELL']);
      App\Central::create(['code' => 'DEW', 'country' => 'GERMANY', 'name' =>	'HELGOLAND']);
      App\Central::create(['code' => 'DKC', 'country' => 'DENMARK', 'name' =>	'COPENHAGEN']);
      App\Central::create(['code' => 'ESA', 'country' => 'SPAIN', 'name' =>	'SAN SEBASTIAN']);
      App\Central::create(['code' => 'ESI', 'country' => 'SPAIN', 'name' =>	'MADRID']);
      App\Central::create(['code' => 'ETM', 'country' => 'ESTONIA', 'name' =>	'MATSALU']);
      App\Central::create(['code' => 'FRP', 'country' => 'FRANCE', 'name' =>	'PARIS']);
      App\Central::create(['code' => 'GBT', 'country' => 'UK & IRELAND', 'name' =>	'LONDON']);
      App\Central::create(['code' => 'GET', 'country' => 'GEORGIA', 'name' =>	'TBLISI']);
      App\Central::create(['code' => 'GRA', 'country' => 'GREECE', 'name' =>	'ATHENS']);
      App\Central::create(['code' => 'HES', 'country' => 'SWITZERLAND', 'name' =>	'SEMPACH']);
      App\Central::create(['code' => 'HGB', 'country' => 'HUNGARY', 'name' =>	'BUDAPEST']);
      App\Central::create(['code' => 'HRZ', 'country' => 'CROATIA', 'name' =>	'ZAGREB']);
      App\Central::create(['code' => 'IAB', 'country' => 'ITALY', 'name' =>	'OZZANO']);
      App\Central::create(['code' => 'ILT', 'country' => 'ISRAEL', 'name' =>	'TEL-AVIV']);
      App\Central::create(['code' => 'ISR', 'country' => 'ICELAND', 'name' =>	'REYKJAVIK']);
      App\Central::create(['code' => 'LIK', 'country' => 'LITHUANIA', 'name' =>	'KAUNAS']);
      App\Central::create(['code' => 'LVR', 'country' => 'LATVIA', 'name' =>	'RIGA']);
      App\Central::create(['code' => 'MEP', 'country' => 'MONTENEGRO', 'name' =>	'PODGORICA']);

    }
}
