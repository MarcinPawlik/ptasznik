<?php

use Illuminate\Database\Seeder;

class SpeciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Species::create(['code' => 'GAT1', 'name' => 'Gatunek 1']);
        App\Species::create(['code' => 'GAT2', 'name' => 'Gatunek 2']);
        App\Species::create(['code' => 'GAT3', 'name' => 'Gatunek 3']);
    }
}
