<?php

use Illuminate\Database\Seeder;

class AgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Age::create(['age' => '1']);
      App\Age::create(['age' => '2']);
      App\Age::create(['age' => '3']);
      App\Age::create(['age' => '4']);
      App\Age::create(['age' => '5']);
      App\Age::create(['age' => '6']);
      App\Age::create(['age' => 'PO1']);
      App\Age::create(['age' => 'PO2']);
      App\Age::create(['age' => 'PO3']);
      App\Age::create(['age' => 'PO3']);
      App\Age::create(['age' => 'PO5']);
      App\Age::create(['age' => 'PO6']);
      App\Age::create(['age' => 'PUL']);
      App\Age::create(['age' => 'L']);

    }
}
