<?php

use Illuminate\Database\Seeder;

class MethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Method::create(['code' => 'W', 'description' =>	'wacek']);
      App\Method::create(['code' => 'N', 'description' =>	'sieć półkowa']);
      App\Method::create(['code' => 'S', 'description' =>	'pułapka sprężynowa (slopek)']);
      App\Method::create(['code' => 'L', 'description' =>	'pętelka lub ręką']);
      App\Method::create(['code' => 'G', 'description' =>	'na gnieździe lub w gnieździe']);
      App\Method::create(['code' => 'V', 'description' =>	'wabienie głosem']);
      App\Method::create(['code' => 'M', 'description' =>	'sieć moździerzowa lub inne mechanizmy nakrywania większej liczby ptaków siecią']);
      App\Method::create(['code' => 'P', 'description' =>	'pułapka chwytająca po jednym osobniku']);

    }
}
