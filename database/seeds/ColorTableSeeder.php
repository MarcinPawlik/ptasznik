<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        App\Color::create(['name' => 'Yellow', 'code' => 'YEL']);
        App\Color::create(['name' => 'Green', 'code' => 'GRE']);
        App\Color::create(['name' => 'Red', 'code' => 'RED']);
        App\Color::create(['name' => 'White', 'code' => 'WHI']);
        App\Color::create(['name' => 'Black', 'code' => 'BLA']);
        App\Color::create(['name' => 'Blue', 'code' => 'BLU']);
        App\Color::create(['name' => 'Pink', 'code' => 'PIN']);
        App\Color::create(['name' => 'Orange', 'code' => 'ORA']);
        App\Color::create(['name' => 'Navy blue', 'code' => 'NBL']);
        App\Color::create(['name' => 'Deep red', 'code' => 'DRE']);
    }
}
