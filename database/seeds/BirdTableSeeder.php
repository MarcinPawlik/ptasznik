<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BirdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for($i=0;$i<10;$i++){
        $bird = new App\Bird();
        $bird->central_id = rand(1,29);
        $bird->species_id = rand(1,3);
        $bird->stat_id = rand(1,12);
        $bird->age_id = rand(1,14);
        $bird->method_id = rand(1,8);
        $bird->color_id = rand(1,10);
        $bird->state_id = rand(1,16);
        $bird->date = Carbon::now();
        $bird->user_id = rand(1,2);
        $bird->save();
      }
    }
}
