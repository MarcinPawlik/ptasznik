<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(['name' => 'Marcin', 'surname' => 'Pawlik', 'email' => 'marcin@wp.pl', 'code' => 'MPA', 'password' => Hash::make('marcin')]);
        App\User::create(['name' => 'Norbert', 'surname' => 'Jurkiewicz', 'email' => 'norbert@wp.pl', 'code' => 'NJU', 'password' => Hash::make('norbert')]);
        //reszte userow/obraczkarzy dodam pozniej
    }
}
