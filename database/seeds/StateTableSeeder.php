<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\State::create(['code' => 'DO', 'name' => 'dolnoslaskie']);
      App\State::create(['code' => 'KP', 'name' => 'kujawsko–pomorskie']);
      App\State::create(['code' => 'BE', 'name' => 'lubelskie']);
      App\State::create(['code' => 'BU', 'name' => 'lubuskie']);
      App\State::create(['code' => 'LO', 'name' => 'łódzkie']);
      App\State::create(['code' => 'MP', 'name' => 'małopolskie']);
      App\State::create(['code' => 'MZ', 'name' => 'mazowieckie']);
      App\State::create(['code' => 'OE', 'name' => 'opolskie']);
      App\State::create(['code' => 'PC', 'name' => 'podkarpackie']);
      App\State::create(['code' => 'PS', 'name' => 'podlaskie']);
      App\State::create(['code' => 'PM', 'name' => 'pomorskie']);
      App\State::create(['code' => 'SL', 'name' => 'slaskie']);
      App\State::create(['code' => 'SW', 'name' => 'swietokrzyskie']);
      App\State::create(['code' => 'WM', 'name' => 'warminsko-mazurskie']);
      App\State::create(['code' => 'WI', 'name' => 'wielkopolskie']);
      App\State::create(['code' => 'ZP', 'name' => 'zachodniopomorskie']);
    }
}
