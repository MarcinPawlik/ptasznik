
@extends('layouts.app')

@section('title', 'Lista ptaków')


@section('script')
<script>
  /* przeniesc do pliku */
  $( document ).ready(function(){
    // $('input').keyup(function() {
    //   $el = $(this)
    //   $name = $el.attr('name');
    //   $value = $el.val();
    //   if($value == ''){
    //     $el.css("border-color", "red");
    //   }else{
    //     $.post( baseUrl+'/bird/ajax', { name: $name, value: $value } ).success(function(data){
    //       if(data == false){
    //         $el.css("border-color", "red");
    //       }else{
    //         $el.css("border-color", "none");
    //       }
    //     });
    //   }
    // });
  });

</script>
@endsection()

@section('content')

@if (isset($errors) && !empty($errors) && count($errors) > 0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
    {{ $error }}<br/>
  @endforeach()
</div>
@endif


    {!! Form::open(['action' => ['BirdController@store'], 'class' => 'form-horizontal']) !!}

    @foreach($inputs as $name_input => $input)
    <div class="form-group col-md-12">
        {!! Form::label($name_input, $input['details']['label'], ['class' => 'col-md-2 control-labe']) !!}
        <div class="col-md-10">
          @if($input['type'] == 'text')
            {!! Form::text($name_input, (Request::has($name_input)?Request::get($name_input):''), ['class' => 'form-control']) !!}
          @elseif($input['type'] == 'select')
            {!! Form::select($name_input, $input['details']['data'], (Request::has($name_input)?Request::get($name_input):''), ['class' => 'form-control']) !!}
          @elseif($input['type'] == 'datetime')
            {!! Form::text($name_input, 'asd', ['class' => 'form-control']) !!}
          @endif()
        </div>
    </div>
    @endforeach()

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">
                Dodaj
            </button>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
