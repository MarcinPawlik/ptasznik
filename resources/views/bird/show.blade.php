
@extends('layouts.app')

@section('title', 'Lista ptaków')

@section('content')

@if (isset($errors) && !empty($errors) && count($errors) > 0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
    {{ $error }}<br/>
  @endforeach()
</div>
@endif


    @foreach($inputs as $name_input => $input)
    <div class="form-group col-md-12">
        {!! Form::label($name_input, $input['details']['label'], ['class' => 'col-md-2 control-labe']) !!}
        <div class="col-md-10">
          @if($input['type'] == 'text')
            {{ $input['details']['value'] }}
          @elseif($input['type'] == 'select')
            {{ $input['details']['data'][$input['details']['value']] }}
          @elseif($input['type'] == 'datetime')
            {{ $input['details']['value'] }}
          @endif()
        </div>
    </div>
    @endforeach()



@endsection
