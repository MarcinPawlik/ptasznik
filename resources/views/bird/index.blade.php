
@extends('layouts.app')

@section('title', 'Lista ptaków')

@section('content')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
@endif

  <a class="btn btn-success pull-right" href="{{ action('BirdController@create') }}">Dodaj</a>
    <table class="table">
      <thead>
        <tr>
          <th>
            #
          </th>
          <th>
            values
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($birds as $bird)
        <tr>
          <td>
            {{ $bird->id }}
          </td>
          <td>
            <!-- relacje -->
            {{ $bird->date->format('Y-m-d H:i:s') }},
            {{ $bird->central->name }},
            {{ $bird->species->id }},
            {{ $bird->stat->id }},
            {{ $bird->age->id }},
            {{ $bird->method->id }},
            {{ $bird->color->id }},
            {{ $bird->state->id }},
            co tutaj wyswietlic ?
          </td>
          <td>
            <a class="btn btn-sm btn-info" href="{{ action('BirdController@show', [$bird->id]) }}">pokaz</a>
            <a class="btn btn-sm btn-primary" href="{{ action('BirdController@edit', [$bird->id]) }}">edytuj</a>
            {!! Form::open(['action' => ['BirdController@destroy', $bird->id], 'method' => 'delete', 'class' => 'form-horizontal']) !!}
              <button class="btn btn-sm btn-danger" type="submit">usuń</button>
            {!! Form::close() !!}
          </form>
          </td>
        </tr>
        @endforeach()
      </tbody>
    </table>
@endsection
