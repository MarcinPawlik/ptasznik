
@extends('layouts.app')

@section('title', 'Lista ptaków')

@section('content')

@if (session('errors'))
<div class="alert alert-danger">
  @foreach(session('errors')->all() as $error)
    {{ $error }}
  @endforeach()
</div>
@endif

<form class="form-horizontal" role="form" method="post" action="{{ action('AgeController@update', $age->id) }}">
  <input name="_method" type="hidden" value="PUT">
    {!! csrf_field() !!}

    <div class="form-group">
        <label class="col-md-2 control-label">Wiek</label>

        <div class="col-md-8">
            <input type="text" class="form-control" name="age" value="{{ $age->age }}">
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-10">
            <button type="submit" class="btn btn-primary pull-right">
                Edytuj
            </button>
        </div>
    </div>
</form>
@endsection
