
@extends('layouts.app')

@section('title', 'Lista ptaków')

@section('content')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
@endif

    <a class="btn btn-success pull-right" href="{{ action('AgeController@create') }}">Dodaj</a>
    <table class="table">
      <thead>
        <tr>
          <th>
            #
          </th>
          <th>
            Nazwa
          </th>
          <th>
            Data utworzenia
          </th>
          <th>
            Data modyfikacji
          </th>
          <th>
            Akcje
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($ages as $age)
        <tr>
          <td>
            {{ $age->id }}
          </td>
          <td>
            {{ $age->age }}
          </td>
          <td>
            {{ $age->created_at->format('Y-m-d H:i') }}
          </td>
          <td>
            {{ $age->updated_at->format('Y-m-d H:i')  }}
          </td>
          <td>
            <a class="btn btn-sm btn-primary" href="{{ action('AgeController@edit', [$age->id]) }}">edytuj</a>
            <form role="form" method="post" action="{{ action('AgeController@destroy', [$age->id]) }}">
              {!! csrf_field() !!}
              <input name="_method" type="hidden" value="DELETE">
              <button class="btn btn-sm btn-danger" type="submit">usun</button>
          </form>
          </td>
        </tr>
        @endforeach()
      </tbody>
    </table>
@endsection
