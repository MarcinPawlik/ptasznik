<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Species extends Model
{

  protected $table = 'species';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'code', 'name',
  ];
}
