<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'age',
  ];
}
