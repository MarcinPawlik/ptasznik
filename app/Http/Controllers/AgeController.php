<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Age;
use App\Bird;

class AgeController extends Controller
{

    function __construct(){
      $this->middleware('auth');
    }

    public function index(Request $req)
    {
        $ages = Age::get();

        return view('age.index')->withAges($ages);
    }

    public function create()
    {
        return view('age.create');
    }

    public function store(Request $req)
    {
        $data = $req->input();
        $validator = $this->rules($data);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->back()->withErrors($errors);
        }

        $age = new Age();
        $age->age = $data['age'];
        $age->save();

        return redirect()->action('AgeController@index')->withSuccess('Dodano wiek');
    }

    public function edit(Request $req, Age $age)
    {
      return view('age.edit')->withAge($age);
    }

    public function update(Request $req, Age $age)
    {
      $data = $req->input();
      $validator = $this->rules($data);

      if ($validator->fails()) {
          $errors = $validator->errors();
          return redirect()->back()->withErrors($errors);
      }

      $age->age = $data['age'];
      $age->save();

      return redirect()->action('AgeController@index')->withSuccess('Zmodyfikowano wiek');
    }

    public function destroy(Age $age)
    {
      if(Bird::where('age_id', $age->id)->exists()){
        return redirect()->action('AgeController@index')->withDanger('Nie można usunąć wieku, ponieważ jest on w użyciu');
      }
      $age->delete();
      return redirect()->action('AgeController@index')->withSuccess('Wiek został usuniety');
    }

    private function rules($data)
    {
        return Validator::make($data, [
              'age' => 'required|max:3',
          ]);
    }
}
