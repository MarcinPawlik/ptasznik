<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Bird;
use App\User;
use App\Central;
use App\Species;
use App\Stat;
use App\Age;
use App\Method;
use App\Color;
use App\State;
use Auth;
use Carbon\Carbon;

class BirdController extends Controller
{

    function __construct(){
      $this->middleware('auth');
    }

    private $inputs = [
      'central_id' => ['type' => 'select', 'details' => ['label' => 'Centrala', 'data' => '', 'value' => '', 'validator' => 'required']],
      'date' => ['type' => 'datetime', 'details' => ['label' => 'Data złpania', 'value' => '', 'validator' => 'required']],
      'nrob' => ['type' => 'text', 'details' => ['label' => 'label1', 'value' => '', 'validator' => 'required|max:8']],
      'species_id' => ['type' => 'select', 'details' => ['label' => '', 'data' => [], 'value' => '', 'validator' => 'required']],
      'stat_id' => ['type' => 'select', 'details' => ['label' => '', 'data' => [], 'value' => '', 'validator' => 'required']],
      'age_id' => ['type' => 'select', 'details' => ['label' => '', 'data' => [], 'value' => '', 'validator' => 'required']],
      'sex' => ['type' => 'select', 'details' => ['label' => '', 'data' => ['M' => 'M', 'K' => 'K'], 'value' => '', 'validator' => 'required']],
      'method_id' => ['type' => 'select', 'details' => ['label' => '', 'data' => '', 'value' => '', 'validator' => 'required']],
      'dcg' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:5']], //!
      'dz' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:5']],
      'wd' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:5']],
      'skok' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:5']],
      'szp' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:4']],
      'skrz' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:3']],
      'wag' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:4']],
      'fat' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:1']],
      'color_id' => ['type' => 'select', 'details' => ['label' => 'Centrala', 'data' => '', 'value' => '', 'validator' => 'required']],
      'kolobr' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:5']],
      'spec1' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:4']],
      'spec2' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:4']],
      'spec3' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:4']],
      'spec4' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:4']],
      'l1' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:10']],
      'l2' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:10']],
      'krew' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:3']],
      'obr' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:30']],
      'msce' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:40']],
      'msce2' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:30']],
      'state_id' => ['type' => 'select', 'details' => ['label' => 'Centrala', 'data' => '', 'value' => '', 'validator' => 'required']],
      'sz' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:10']],
      'dl' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|numeric|max:10']],
      'uwagi' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:255']],
      'dctrl' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:10']],
      'dnrob' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:8']],
      'lp' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:2']],
      'rnrob' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:8']],
      'r1ctrl' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:10']],
      'r1nrob' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:7']],
      'r2ctrl' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:10']],
      'r2nrob' => ['type' => 'text', 'details' => ['label' => '', 'value' => '', 'validator' => 'required|max:8']],
    ];

    public function index(){
      $birds = Bird::get();
      return view('bird.index')->withBirds($birds);
    }

    public function show(Bird $bird){
      $this->addToInputs($this->inputs);
      foreach($this->inputs as $key => $input){
        if($this->inputs[$key]['type'] == 'datetime'){
          $this->inputs[$key]['details']['value'] = $bird->$key->format('Y-m-d H:i:s');
        }else{
          $this->inputs[$key]['details']['value'] = $bird->$key;
        }
      }
      return view('bird.show')->withBird($bird)->withInputs($this->inputs);
    }

    public function create()
    {
        $this->addToInputs($this->inputs);
        return view('bird.create')->withInputs($this->inputs);
    }

    public function store(Request $req)
    {
        $data = $req->input();
        $validator = $this->rules($data);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->create()->withErrors($errors);
        }

        $data['date'] = Carbon::now(); //zmienic
        Bird::create($data); //dodac pozniej id_user

        return redirect()->action('BirdController@index')->withSuccess('Dodano ptaka');
    }

    public function edit(Bird $bird)
    {
      $this->addToInputs($this->inputs);
      foreach($this->inputs as $key => $input){
        if($this->inputs[$key]['type'] == 'datetime'){
          $this->inputs[$key]['details']['value'] = $bird->$key->format('Y-m-d H:i:s');
        }else{
          $this->inputs[$key]['details']['value'] = $bird->$key;
        }
      }
      return view('bird.edit')->withBird($bird)->withInputs($this->inputs);
    }

    public function update(Request $req, Bird $bird)
    {
      $data = $req->input();
      $validator = $this->rules($data);

      if ($validator->fails()) {
          $errors = $validator->errors();
          return $this->edit($bird)->withErrors($errors);
      }

      $data['date'] = Carbon::now(); //zmienic
      $bird->update($data);

      return redirect()->action('BirdController@index')->withSuccess('Zmodyfikowano ptaka');
    }

    public function destroy(Bird $bird)
    {

      $bird->delete();
      return redirect()->action('BirdController@index')->withSuccess('Ptak został usuniety');
    }

    public function ajax(Request $req){ //pozniej przeniesc do osobnego kontrollera a $this->input wrzucic do modelu
      $name = $req->input('name');
      $value = $req->input('value');

      $rules = [];
      foreach($this->inputs as $key => $input){
        if($key == $name){
          $rules[$key] = $input['details']['validator'];
        }
      }

      $validator = Validator::make([$name => $value], $rules);
      if ($validator->fails()) {
          // $errors = $validator->errors();
          // dd($errors);
          return response()->json(false);
      }
      return response()->json(true);
    }

    private function rules($data)
    {
      $rules = [];
      foreach($this->inputs as $key => $input){
        $rules[$key] = $input['details']['validator'];
      }
      return Validator::make($data, $rules);
    }

    private function addToInputs(&$inputs){
      $this->inputs['central_id']['details']['data'] = Central::lists('name', 'id')->toArray();
      $this->inputs['species_id']['details']['data'] = Species::lists('name', 'id')->toArray();
      $this->inputs['stat_id']['details']['data'] = Stat::lists('code', 'id')->toArray();
      $this->inputs['age_id']['details']['data'] = Age::lists('age', 'id')->toArray();
      $this->inputs['method_id']['details']['data'] = Method::lists('code', 'id')->toArray();
      $this->inputs['color_id']['details']['data'] = Color::lists('name', 'id')->toArray();
      $this->inputs['state_id']['details']['data'] = State::lists('name', 'id')->toArray();
    }
}
