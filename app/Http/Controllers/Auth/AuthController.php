<?php

namespace App\Http\Controllers\Auth;

use App\User;
//use Validator;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{

  function __construct(){
    //$this->middleware('guest');
  }


    public function index(){
      $this->middleware('guest');
      return view('auth.login');
    }

    public function login(Request $request){
      $this->middleware('guest');
      $login = $request->input('login');
      $password = $request->input('password');
      if (Auth::attempt(['email' => $login, 'password' => $password], true)) {
          //dd(Auth::user());
          return redirect()->action('HomeController@index');
      }
      return view('auth.login');
    }

    public function logout(){
      $this->middleware('auth');
      Auth::logout();
      return redirect()->action('HomeController@index');
    }


}
