<?php



Route::post('/bird/ajax', 'BirdController@ajax'); //without token

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/auth/login', '\App\Http\Controllers\Auth\AuthController@index');
    Route::post('/auth/login', '\App\Http\Controllers\Auth\AuthController@login');
    Route::get('/auth/logout', '\App\Http\Controllers\Auth\AuthController@logout');

    Route::resource('age', 'AgeController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
    Route::resource('bird', 'BirdController', ['only' => ['show', 'index', 'create', 'store', 'edit', 'update', 'destroy']]);

});
