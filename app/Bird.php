<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bird extends Model
{
    protected $dates = ['date', 'created_at', 'updated_at'];

    protected $fillable = [
      'central_id',
      'date',
      'nrob',
      'species_id',
      'stat_id',
      'age_id',
      'sex',
      'method_id',
      'dcgm',
      'dz',
      'wd',
      'skok',
      'szp',
      'skrz',
      'wag',
      'fat',
      'color_id',
      'kolobr',
      'spec1',
      'spec2',
      'spec3',
      'spec4',
      'l1',
      'l2',
      'krew',
      'obr',
      'msce',
      'msce2',
      'state_id',
      'sz',
      'dl',
      'uwagi',
      'dctrl',
      'dnrob',
      'lp',
      'rnrob',
      'r1ctrl',
      'r1nrob',
      'r2ctrl',
      'r2nrob',
    ];

    public function central()
    {
        return $this->hasOne('App\Central', 'id', 'central_id');
    }

    public function species()
    {
        return $this->hasOne('App\Species', 'id', 'species_id');
    }

    public function stat()
    {
        return $this->hasOne('App\Stat', 'id', 'stat_id');
    }

    public function age()
    {
        return $this->hasOne('App\Age', 'id', 'age_id');
    }

    public function method()
    {
        return $this->hasOne('App\Method', 'id', 'method_id');
    }

    public function color()
    {
        return $this->hasOne('App\Color', 'id', 'color_id');
    }

    public function state()
    {
        return $this->hasOne('App\State', 'id', 'state_id');
    }




}
